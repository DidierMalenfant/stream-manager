#
# Copyright (c) 2022-present Didier Malenfant <didier@malenfant.net>
#
# This file is part of StreamManager.
#
# StreamManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# StreamManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License along with StreamManager. If not,
# see <https://www.gnu.org/licenses/>.
#

# TODO: Document code.
# TODO: Better error handling.

try:
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib

import getopt
import os
import sys
import traceback
import StreamDeckLayoutManager

from pathlib import Path
from typing import List, Optional

from .MastodonClient import MastodonClient
from .OBSClient import OBSClient
from .TraktorClient import TraktorClient
from .CueFileClient import CueFileClient
from .PostClient import PostClient


def printUsage() -> None:
    print('usage: streammanager <-t> <-m> <-c config.toml> <--cues cue_output_filename')


def main() -> None:
    obs_client: Optional[OBSClient] = None
    traktor_client: Optional[TraktorClient] = None
    streamdeck_client: Optional[StreamDeckLayoutManager.Manager] = None
    mastodon_client: Optional[MastodonClient] = None
    cuefile_client: Optional[CueFileClient] = None

    config_file_path: Optional[str] = None
    post_updates_masto = False
    test_post = False

    try:
        # -- Gather the arguments but ignore the first argument (which is the script filename)
        opts, other_arguments = getopt.getopt(sys.argv[1:], 'c:mt', ['cues='])

        if len(opts):
            # -- Iterate over the options and values
            for opt, arg_val in opts:
                if opt == '-c':
                    config_file_path = arg_val
                elif opt == '-m':
                    print('Enabling Mastodon updates...')
                    post_updates_masto = True
                elif opt == '-t':
                    test_post = True
                elif opt == '--cues':
                    cuefile_client = CueFileClient(os.path.expanduser(arg_val))
    except getopt.GetoptError:
        printUsage()
        sys.exit(0)

    if config_file_path is None:
        print('Couldn\'t find any ini file path on the command line.')
        printUsage()
        sys.exit(2)

    print('Reading configuration...')

    if not os.path.exists(config_file_path):
        print(f'Can\'t read toml file at \'{config_file_path}\'.')
        sys.exit(2)

    with open(config_file_path, mode="rb") as fp:
        config = tomllib.load(fp)

    post_clients: List[PostClient] = []

    if post_updates_masto:
        mastodon_client = MastodonClient(config['mastodon'], config['posts'])
        post_clients.append(mastodon_client)

    if cuefile_client is not None:
        post_clients.append(cuefile_client)

    if test_post:
        for client in post_clients:
            client.post_start_text()
            client.post_status("Test Track", "Test Artist", "TestLabel")
            client.post_status("Test Track", "Test Artist")
            client.post_stop_text()

        sys.exit(0)

    streamdeck_manager: Optional[StreamDeckLayoutManager.Manager] = None

    try:
        streamdeck_manager = StreamDeckLayoutManager.Manager(os.path.join(Path(__file__).parent, 'StreamDeckConfig.toml'))
        streamdeck_manager.displayPage('MainPage')
    except Exception as e:
        print(e)

    traktor_client = TraktorClient(config['traktor'], streamdeck_manager, post_clients)
    obs_client = OBSClient(config['obs'], streamdeck_manager, post_clients)

    try:
        if traktor_client is not None:
            traktor_client.start()
    except Exception:
        print(traceback.format_exc())
    except KeyboardInterrupt:
        print('Execution interrupted by user.')
        pass

    if streamdeck_client is not None:
        streamdeck_client.shutdown()

    if obs_client is not None:
        obs_client.shutdown()


if __name__ == '__main__':
    main()
