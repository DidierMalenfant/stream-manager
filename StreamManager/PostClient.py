#
# Copyright (c) 2022-present Didier Malenfant <didier@malenfant.net>
#
# This file is part of StreamManager.
#
# StreamManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# StreamManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License along with StreamManager. If not,
# see <https://www.gnu.org/licenses/>.
#

from typing import Optional


# -- Classes
class PostClient:
    """Manage all our Mastodon interactions."""

    def post_start_text(self) -> None:
        """Post the stream start text."""
        raise RuntimeError('Calling virtual method.')

    def post_stop_text(self) -> None:
        """Post the stream stop text."""
        raise RuntimeError('Calling virtual method.')

    def post_status(self, title: str, artist: str, label: Optional[str] = None, artwork_filename: Optional[str] = None) -> None:
        """Post the currently playing track."""
        raise RuntimeError('Calling virtual method.')
