#
# Copyright (c) 2022-present Didier Malenfant <didier@malenfant.net>
#
# This file is part of StreamManager.
#
# StreamManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# StreamManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License along with StreamManager. If not,
# see <https://www.gnu.org/licenses/>.
#

import os
import sys

try:
    import magic
except Exception as e:
    print(e)
    print('You may need to install libmagic\'s binary (brew install libmagic).')
    sys.exit(1)

from mastodon import Mastodon
from typing import Dict, List, Optional

from .PostClient import PostClient


# -- Classes
class MastodonClient(PostClient):
    """Manage all our Mastodon interactions."""

    def __init__(self, config: Dict[str, str], posts: Dict[str, str]):
        """Initialize the client based on user configuration."""

        print('Setting up Mastodon...')

        self._stream_start_text = posts['StreamStartText']
        self._stream_stop_text = posts['StreamStopText']
        self._track_update_text = posts['TrackUpdateText']
        self._track_update_no_label_text = posts['TrackUpdateNoLabelText']

        self._last_toot_status_id: Optional[str] = None

        self._client = Mastodon(client_id=config['ClientID'],
                                client_secret=config['ClientSecret'],
                                access_token=config['AccessToken'],
                                api_base_url=config['APIBaseURL'])

    def toot(self, text: str, in_reply_to: Optional[str] = None, visibility: str = 'public', media_filename: Optional[str] = None) -> Optional[str]:
        """Toot some text."""

        if self._client is None:
            return None

        result: Dict[str, str] = {}

        # -- Update the status
        try:
            media_ids: Optional[List[str]] = None

            if media_filename is not None and os.path.exists(media_filename):
                mime = magic.Magic(mime=True)
                mime_type = mime.from_file(media_filename)

                with open(media_filename, 'rb') as f:
                    content = f.read()

                    media = self._client.media_post(media_file=content,
                                                    mime_type=mime_type,
                                                    description="Cover art of the track currently playing.")
                    media_ids = [media["id"]]

                    f.close()

            result = self._client.status_post(text,
                                              in_reply_to_id=in_reply_to,
                                              media_ids=media_ids,
                                              visibility=visibility)
        except Exception as e:
            print(f'Error tooting!: {text}')
            print(f'-=> {e}')
            return None

        return result.get("id")

    def post_start_text(self) -> None:
        """Toot the stream start text."""
        self._last_toot_status_id = self.toot(self._stream_start_text)

    def post_stop_text(self) -> None:
        """Toot the stream stop text."""
        self.toot(self._stream_stop_text, in_reply_to=self._last_toot_status_id)
        self._last_toot_status_id = None

    def post_status(self, title: str, artist: str, label: Optional[str] = None, artwork_filename: Optional[str] = None) -> None:
        """Toot the currently playing track."""

        if len(title) == 0 or len(artist) == 0:
            return

        update_message = ''

        if label is None or len(label) == 0:
            update_message = self._track_update_no_label_text.replace('{title}', title)
            update_message = update_message.replace('{artist}', artist)
        else:
            update_message = self._track_update_text.replace('{title}', title)
            update_message = update_message.replace('{artist}', artist)
            update_message = update_message.replace('{label}', label)

        if artwork_filename is not None and os.path.exists(artwork_filename):
            self._last_toot_status_id = self.toot(text=update_message,
                                                  media_filename=artwork_filename,
                                                  in_reply_to=self._last_toot_status_id)
        else:
            self._last_toot_status_id = self.toot(text=update_message,
                                                  in_reply_to=self._last_toot_status_id)
