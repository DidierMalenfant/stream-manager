#
# Copyright (c) 2022-present Didier Malenfant <didier@malenfant.net>
#
# This file is part of StreamManager.
#
# StreamManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# StreamManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License along with StreamManager. If not,
# see <https://www.gnu.org/licenses/>.
#

import obswebsocket
import StreamDeckLayoutManager

from threading import Thread
from typing import Dict, List, Optional, Callable

from .PostClient import PostClient


# -- Classes
class OBSClient:
    """Manage all our OBS interactions."""

    def __init__(self, config: Dict[str, str], streamdeck_manager: Optional[StreamDeckLayoutManager.Manager], post_clients: List[PostClient]):
        """Initialize the client based on user configuration."""

        print('Setting up OBS...')

        self._streamdeck_manager = streamdeck_manager
        self._post_clients = post_clients

        self._stream_status_key = int(config['StreamStatusKey'])
        self._record_status_key = int(config['RecordStatusKey'])

        self._scene_selection_keys: List[int] = []
        self._current_scene_index = 0

        self._obs = obswebsocket.obsws(config['ObsServerAddress'], int(config['ObsServerPort']), config['ObsServerPassword'])
        self._obs.register(self._onTransition, obswebsocket.events.TransitionBegin)
        self._obs.register(self._onSceneChanged, obswebsocket.events.TransitionEnd)
        self._obs.register(self._onStreamStateChanged, obswebsocket.events.StreamStateChanged)
        self._obs.register(self._onRecordStateChanged, obswebsocket.events.RecordStateChanged)

        try:
            self._obs.connect()
        except obswebsocket.exceptions.ConnectionFailure:
            print('ERROR: Cannot connect to OBS.')
            self._obs = None
            return

        # -- Get all the scenes
        all_scenes = self._obs.call(obswebsocket.requests.GetSceneList())

        self._scenes: List[str] = []
        for scene in all_scenes.getScenes():
            self._scenes.append(scene['sceneName'])

        if self._streamdeck_manager is not None:
            self._streamdeck_manager.setCallback('obs', self._streamdeckCallback)

        index = 0
        for key_index_as_string in config['SceneSelectionKeys']:
            key_index = int(key_index_as_string)

            self._scene_selection_keys.append(key_index)

            self._setSceneOffAtIndex(index)
            index += 1

        # -- Update the current scene
        self._updateCurrentSceneIndex()
        self._setCurrentSceneKey(True)

        # -- Update the initial stream status
        status = self._obs.call(obswebsocket.requests.GetStreamStatus())
        self._stream_on = status.getOutputActive()

        status = self._obs.call(obswebsocket.requests.GetRecordStatus())
        self._record_on = status.getOutputActive()

        self._in_transition = False

        self._updateStreamStatusKey()
        self._updateRecordStatusKey()

    def _updateCurrentSceneIndex(self) -> None:
        if self._obs is None:
            return

        current_scene = self._obs.call(obswebsocket.requests.GetCurrentProgramScene())
        self._current_scene_index = self._scenes.index(current_scene.getSceneName())

    def _setSceneOffAtIndex(self, index: int) -> None:
        if index < 0 or index >= len(self._scene_selection_keys) or index >= len(self._scenes):
            return

        if self._streamdeck_manager is not None:
            self._streamdeck_manager.setKey('MainPage', self._scene_selection_keys[index],
                                            'camera_off.png', self._scenes[index],
                                            StreamDeckLayoutManager.CallbackCall(['obs', 'select_scene', index]))

    def _setCurrentSceneKey(self, on_or_off: bool) -> None:
        if on_or_off:
            if self._streamdeck_manager is not None:
                self._streamdeck_manager.setKey('MainPage', self._scene_selection_keys[self._current_scene_index],
                                                'camera_on.png', self._scenes[self._current_scene_index],
                                                StreamDeckLayoutManager.CallbackCall(['obs', 'select_scene', self._current_scene_index]))
        else:
            self._setSceneOffAtIndex(self._current_scene_index)

    def _updateStreamStatusKey(self) -> None:
        if self._streamdeck_manager is not None:
            if self._stream_on:
                self._streamdeck_manager.setKey('MainPage', self._stream_status_key,
                                                'stream_on.png', 'Live',
                                                StreamDeckLayoutManager.CallbackCall(['obs', 'set_stream', False]))
            else:
                self._streamdeck_manager.setKey('MainPage', self._stream_status_key,
                                                'stream_off.png', 'Offline',
                                                StreamDeckLayoutManager.CallbackCall(['obs', 'set_stream', True]))

    def _updateRecordStatusKey(self) -> None:
        if self._streamdeck_manager is not None:
            if self._record_on:
                self._streamdeck_manager.setKey('MainPage', self._record_status_key,
                                                'stream_on.png', 'Record On',
                                                StreamDeckLayoutManager.CallbackCall(['obs', 'set_record', False]))
            else:
                self._streamdeck_manager.setKey('MainPage', self._record_status_key,
                                                'stream_off.png', 'Record Off',
                                                StreamDeckLayoutManager.CallbackCall(['obs', 'set_record', True]))

    def _setStreamStatus(self, call: StreamDeckLayoutManager.CallbackCall) -> None:
        if call.numberOfArguments() != 2:
            return

        status = call.argumentAsBoolean(at_index=1)
        if not status and self._stream_on:
            self.stopStreaming()
        elif status and not self._stream_on:
            self.startStreaming()

    def _setRecordStatus(self, call: StreamDeckLayoutManager.CallbackCall) -> None:
        print(call.numberOfArguments())
        if call.numberOfArguments() != 2:
            return

        status = call.argumentAsBoolean(at_index=1)
        print(f'Set Record {status} {self._record_on}')
        if not status and self._record_on:
            print(f'Stop Record {status} {self._record_on}')
            self.stopRecording()
        elif status and not self._record_on:
            print(f'Start Record {status} {self._record_on}')
            self.startRecording()

    def _setCurrentScene(self, call: StreamDeckLayoutManager.CallbackCall) -> None:
        if self._obs is None:
            return

        if self._in_transition:
            return

        if call.numberOfArguments() != 2:
            return

        scene_index = call.argumentAsInteger(at_index=1)
        if scene_index >= len(self._scenes) or scene_index == self._current_scene_index:
            return

        self._obs.call(obswebsocket.requests.SetCurrentProgramScene(sceneName=self._scenes[scene_index]))

    def _onTransition(self, event: obswebsocket.events.TransitionBegin) -> None:
        self._in_transition = True

    def _doObsSceneChanged(self) -> None:
        self._setCurrentSceneKey(False)
        self._updateCurrentSceneIndex()
        self._setCurrentSceneKey(True)

        self._in_transition = False

    def _onSceneChanged(self, event: obswebsocket.events.TransitionEnd) -> None:
        Thread(target=self._doObsSceneChanged).start()

    def _onStreamStateChanged(self, event: obswebsocket.events.StremStateChanged) -> None:
        self._stream_on = event.getOutputActive()

        self._updateStreamStatusKey()

        if self._stream_on:
            for client in self._post_clients:
                client.post_start_text()
        else:
            for client in self._post_clients:
                client.post_stop_text()

    def _onRecordStateChanged(self, event: obswebsocket.events.RecordStateChanged) -> None:
        self._record_on = event.getOutputActive()

        self._updateRecordStatusKey()

    def _streamdeckCallback(self, call: StreamDeckLayoutManager.CallbackCall) -> None:
        switch: Dict[str, Callable[[StreamDeckLayoutManager.CallbackCall], None]] = {
            'set_stream': self._setStreamStatus,
            'set_record': self._setRecordStatus,
            'select_scene': self._setCurrentScene
        }

        if call.numberOfArguments() == 0:
            return

        method = switch.get(call.argumentAsString(at_index=0))
        if method is not None:
            method(call)

    def startStreaming(self) -> None:
        if self._obs is None:
            return

        print('Set Stream ON')
        self._obs.call(obswebsocket.requests.StartStream())

    def stopStreaming(self) -> None:
        if self._obs is None:
            return

        print('Set Stream OFF')
        self._obs.call(obswebsocket.requests.StopStream())

    def startRecording(self) -> None:
        if self._obs is None:
            return

        print('Set Recording ON')
        self._obs.call(obswebsocket.requests.StartRecord())

    def stopRecording(self) -> None:
        if self._obs is None:
            return

        print('Set Recording OFF')
        self._obs.call(obswebsocket.requests.StopRecord())

    def shutdown(self) -> None:
        print('Shutting down obs...')

        if self._obs is not None:
            self._obs.disconnect()

            self._obs = None
