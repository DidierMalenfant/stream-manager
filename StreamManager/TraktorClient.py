#
# Copyright (c) 2022-present Didier Malenfant <didier@malenfant.net>
#
# This file is part of StreamManager.
#
# StreamManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# StreamManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License along with StreamManager. If not,
# see <https://www.gnu.org/licenses/>.
#

import os
import mido
import TraktorBuddy
import StreamDeckLayoutManager

from pathlib import Path
from threading import Thread
from time import sleep
from PIL import Image
from typing import Dict, List, Any, Callable, Optional

from .PostClient import PostClient


# -- Functions
def _callAtInterval(period: int, callback: Callable[[], None], args: List[Any]) -> None:
    while True:
        sleep(period)
        callback(*args)


def _setInterval(period: int, callback: Callable[[], None], *args: List[Any]) -> None:
    Thread(target=_callAtInterval, args=(period, callback, args)).start()


# -- Classes
class TraktorClient:
    """Manage all our Traktor interactions."""

    def __init__(self, config: Dict[str, Any], streamdeck_manager: Optional[StreamDeckLayoutManager.Manager], post_clients: List[PostClient]):
        """Initialize the client based on user configuration."""

        print('Setting up Traktor...')
        self._assets_folder = os.path.join(Path(__file__).parent, 'assets')
        self._streamdeck_manager = streamdeck_manager
        self._post_clients = post_clients
        self._extra_text_filename = os.path.expanduser(config['OutputExtraTextFilename'])
        self._playing_track_title_filename = os.path.expanduser(config['OutputTitleFilename'])
        self._playing_track_title_max_length = int(config['OutputTitleMaxLength'])
        self._extra_text = config['OutputExtraText']
        self._playing_track_artist_filename = os.path.expanduser(config['OutputArtistFilename'])
        self._playing_track_artist_prefix = config['OutputArtistPrefix']
        self._playing_track_artist_max_length = int(config['OutputArtistMaxLength'])
        self._playing_track_label_filename = os.path.expanduser(config['OutputLabelFilename'])
        self._playing_track_label_prefix = config['OutputLabelPrefix']
        self._playing_track_label_max_length = int(config['OutputLabelMaxLength'])
        self._playing_track_artwork_filename = os.path.expanduser(config['OutputArtworkFilename'])
        self._playing_track_fullres_artwork_filename = os.path.expanduser(config['OutputFulleResArtworkFilename'])
        self._no_artwork_placeholder_filename = os.path.join(self._assets_folder, 'vinyl_disc.png')
        self._button_artwork_filename = os.path.expanduser(config['ButtonArtworkTempFilename'])
        self._new_track_available_key = int(config['NewTrackAvailableKey'])
        self._clear_current_track_key = int(config['ClearCurrentTrackKey'])
        self._skip_next_track_key = int(config['SkipNextTrackKey'])

        self._traktor_collection: Optional[TraktorBuddy.Collection] = None
        self._midi: Optional[Any] = None

        self._current_track: Optional[TraktorBuddy.Track] = None
        self._next_track: Optional[TraktorBuddy.Track] = None

        self._current_extra_text = ''

        midi_port_name = config.get('VirtualMidiInputPortName', None)
        if midi_port_name is not None:
            try:
                self._midi = mido.open_output(midi_port_name)
            except Exception as e:
                print(f'EROR: {str(e)}')
                print('Found midi ports:')
                for name in mido.get_output_names():
                    print(f'   {name}')

        if self._streamdeck_manager is not None:
            self._streamdeck_manager.setCallback('midi', self._midiCallback)
            self._streamdeck_manager.setCallback('now_playing', self._nowPlayingCallback)

    def _updateMetadata(self, track: TraktorBuddy.Track) -> None:
        print(f'Available: {track.title()} {track.artist()}')

        self._next_track = track

    def _updateTrackArtwork(self, track: Optional[TraktorBuddy.Track], destination_image_filename: str, full_res: bool = False, no_placeholder: bool = False) -> None:
        if track is None:
            if os.path.exists(destination_image_filename):
                os.remove(destination_image_filename)

            return

        image: Optional[Image.Image] = None
        tried_placeholder = False

        image = track.coverArtImageFromFile()
        if image is None:
            image = track.coverArtImageFromCache()

        while True:
            try:
                if image is None:
                    if tried_placeholder is True or no_placeholder is True:
                        if os.path.exists(destination_image_filename):
                            os.remove(destination_image_filename)

                        return

                    image = Image.open(self._no_artwork_placeholder_filename)
                    tried_placeholder = True

                if not full_res and image.size != (125, 125):
                    image = image.resize((125, 125))

                if image.mode != 'RGB' and image.mode != 'RGBA':
                    image = image.convert(mode='RGB')

                image.save(destination_image_filename, 'PNG')
                image.close()
            except Exception as e:
                print(f'Exception: {e}')
                image = None

            if image is not None:
                break

    def _updateTrackString(self) -> None:
        Path(self._extra_text_filename).write_text(f'{self._current_extra_text}')

        title = '' if self._current_track is None else self._current_track.title()
        if title is None:
            title = ''
        else:
            if len(title) > self._playing_track_title_max_length:
                title = title[:self._playing_track_title_max_length - 3] + '...'

        Path(self._playing_track_title_filename).write_text(f'{title}')

        artist = ''
        if self._current_track is not None:
            current_artist = self._current_track.artist()
            if current_artist is not None:
                artist = current_artist

        if len(artist) > self._playing_track_artist_max_length:
            artist = artist[:self._playing_track_artist_max_length - 3] + '...'

        if len(artist) != 0 and self._playing_track_artist_prefix is not None and len(self._playing_track_artist_prefix) != 0:
            artist = self._playing_track_artist_prefix + ' ' + artist

        Path(self._playing_track_artist_filename).write_text(artist)

        label = ''
        if self._current_track is not None:
            current_label = self._current_track.label()
            if current_label is not None:
                label = current_label

        if len(label) > self._playing_track_label_max_length:
            label = label[:self._playing_track_label_max_length - 3] + '...'

        if len(label) != 0:
            if self._playing_track_label_prefix is not None and len(self._playing_track_label_prefix) != 0:
                label = self._playing_track_label_prefix + ' ' + label
            else:
                label = f'[{label}]'

        Path(self._playing_track_label_filename).write_text(label)

        print(f'Output: {title} {artist} {label}')

    def _checkForNewTracks(self) -> None:
        if self._next_track is not None:
            if self._streamdeck_manager is not None:
                self._streamdeck_manager.setKey('MainPage', self._skip_next_track_key,
                                                'skip_track.png', 'Skip',
                                                StreamDeckLayoutManager.CallbackCall(['now_playing', 'skip_track']))
            else:
                self.postNewTrack()
                return

            if not os.path.exists(self._button_artwork_filename):
                self._updateTrackArtwork(self._next_track, self._button_artwork_filename)

            if self._streamdeck_manager is not None:
                self._streamdeck_manager.setKey('MainPage', self._new_track_available_key,
                                                self._button_artwork_filename, 'Post',
                                                StreamDeckLayoutManager.CallbackCall(['now_playing', 'new_track']))
        else:
            if os.path.exists(self._button_artwork_filename):
                os.remove(self._button_artwork_filename)

            if self._streamdeck_manager is not None:
                self._streamdeck_manager.setKey('MainPage', self._new_track_available_key, None, None, None)
                self._streamdeck_manager.setKey('MainPage', self._skip_next_track_key, None, None, None)

                if self._current_track is None:
                    self._streamdeck_manager.setKey('MainPage', self._clear_current_track_key, None, None, None)

    def _midiCallback(self, call: StreamDeckLayoutManager.CallbackCall) -> None:
        if call.numberOfArguments() == 0 or self._midi is None:
            return

        command = call.argumentAsString(at_index=0)
        if command == 'note_on':
            self._midi.send(mido.Message('note_on', note=call.argumentAsString(at_index=1), channel=call.argumentAsString(at_index=2), velocity=call.argumentAsString(at_index=3)))
        elif command == 'note_off':
            self._midi.send(mido.Message('note_off', note=call.argumentAsString(at_index=1), channel=call.argumentAsString(at_index=2)))

    def _nowPlayingCallback(self, call: StreamDeckLayoutManager.CallbackCall) -> None:
        if call.numberOfArguments() != 1:
            return

        switch = {
            'new_track': self.postNewTrack,
            'clear_track': self.clearCurrentTrack,
            'skip_track': self.skipNextTrack
        }

        method = switch.get(call.argumentAsString(at_index=0))
        if method is None:
            return

        method()

    def start(self) -> None:
        self._updateTrackString()
        self._updateTrackArtwork(self._current_track, self._playing_track_artwork_filename)
        self._updateTrackArtwork(self._current_track, self._playing_track_fullres_artwork_filename, full_res=True, no_placeholder=True)

        _setInterval(1, self._checkForNewTracks)

        self._traktor_collection = TraktorBuddy.Collection()

        print('Listening to Traktor...')
        listener = TraktorBuddy.Listener(self._traktor_collection, self._updateMetadata)
        listener.start()

    def postNewTrack(self) -> None:
        if self._next_track is None:
            return

        self._current_extra_text = self._extra_text

        self._current_track = self._next_track
        self._next_track = None

        self._updateTrackString()
        self._updateTrackArtwork(self._current_track, self._playing_track_artwork_filename)
        self._updateTrackArtwork(self._current_track, self._playing_track_fullres_artwork_filename, full_res=True, no_placeholder=True)

        if self._streamdeck_manager is not None:
            self._streamdeck_manager.setKey('MainPage', self._clear_current_track_key,
                                            'clear_track.png', 'Clear',
                                            StreamDeckLayoutManager.CallbackCall(['now_playing', 'clear_track']))

        title = self._current_track.title()
        if title is None:
            title = ''
        artist = self._current_track.artist()
        if artist is None:
            artist = ''
        label = self._current_track.label()

        for client in self._post_clients:
            client.post_status(title, artist, label, self._playing_track_fullres_artwork_filename)

    def clearCurrentTrack(self) -> None:
        if self._current_track is None:
            return

        print('Clearing Track Name')

        self._current_extra_text = ''

        self._current_track = None

        self._updateTrackString()
        self._updateTrackArtwork(self._current_track, self._playing_track_artwork_filename)
        self._updateTrackArtwork(self._current_track, self._playing_track_fullres_artwork_filename, full_res=True, no_placeholder=True)

    def skipNextTrack(self) -> None:
        if self._next_track is None:
            return

        print('Skipping Next Track')

        self._current_extra_text = ''

        self._updateTrackString()
        self._updateTrackArtwork(self._current_track, self._playing_track_artwork_filename)
        self._updateTrackArtwork(self._current_track, self._playing_track_fullres_artwork_filename, full_res=True, no_placeholder=True)
