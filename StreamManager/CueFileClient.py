#
# Copyright (c) 2022-present Didier Malenfant <didier@malenfant.net>
#
# This file is part of StreamManager.
#
# StreamManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# StreamManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License along with StreamManager. If not,
# see <https://www.gnu.org/licenses/>.
#

from timeit import default_timer as timer
from typing import Optional

from .PostClient import PostClient


# -- Classes
class CueFileClient(PostClient):
    """Manage all our Mastodon interactions."""

    def __init__(self, output_filepath: str):
        """Initialize the client based on user configuration."""

        print(f'Outputting cues to \'{output_filepath}\'.')

        self._start_time = 0
        self._output_filepath = output_filepath

    def post_start_text(self) -> None:
        """Start the timer."""
        self._start_time = int(timer())

        # -- Reset the CUE file
        with open(self._output_filepath, "w") as file:
            file.close()

    def post_stop_text(self) -> None:
        """Reset the timer."""
        self._start_time = 0

    def post_status(self, title: str, artist: str, label: Optional[str] = None, artwork_filename: Optional[str] = None) -> None:
        """Add a mark for the current track."""
        if len(title) == 0 or len(artist) == 0:
            return

        if self._start_time == 0:
            # -- Reset the CUE file
            with open(self._output_filepath, "w") as file:
                file.close()

            self._start_time = int(timer())

        elapsed_seconds = int(timer()) - self._start_time
        elapsed_hours = elapsed_seconds // 3600
        elapsed_seconds %= 3600
        elapsed_minutes = elapsed_seconds // 60
        elapsed_seconds %= 60

        output_string = f'{artist.replace("-", "_")} - {title.replace("-", "_")} {elapsed_hours:02d}:{elapsed_minutes:02d}:{elapsed_seconds:02d}'
        print(f'Cue File output: {output_string}')

        with open(self._output_filepath, "a") as file:
            print(output_string, file=file)
