# Stream Manager

A little Swiss army knife script for my live streams.

# Traktor setup

Add a Generic Midi controller and select `Traktor Virtual Input` as input and `Traktor Virtual Output` as output.

In Broadcasting, set server path to `localhost`, port to `8000`, mount path to `/`, password empty and format to the lowest frequency and bitrate.

# OBS setup

You need 27+ in order to use the embedded web socket server. Server settings can be written to the `obs` section of the config file:

# Streamdeck setup

The manager uses the configuration found in its local `StreamDeckConfig.toml` for now.

### License

**StreamManager** is distributed under the terms of the [GPLv3.0](https://spdx.org/licenses/GPL-3.0-or-later.html) or later license.
